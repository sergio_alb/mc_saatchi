var submit = document.getElementById("submit");
  submit.addEventListener("click", function (){
    validation();
  });

  function validation() {
    var name, surname, phone, telephone, mail, valName, valSurname, valPhone, valMail;
  
    //messages
    msgRequired = 'Campo obbligatorio';
    msgName = 'Nome non valido';
    msgSurname = 'Cognome non valido';
    msgNumber = 'Inserire almeno un numero telefonico';
    msgPhone = 'Numero non valido';
    msgCell = 'Numero di cellulare non valido';
    msgmail = 'Email non valida';
    
    //regex
    var letters = /^[A-Za-z]+$/;
    var phoneRegex = /^\d+$/;
    var mailRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

    name = document.getElementById("name");
    surname = document.getElementById("surname");
    telephone = document.getElementById("telephone");
    phone = document.getElementById("phone");
    mail = document.getElementById("mail");
    valName = false;
    valSurname = false;
    valPhone = this.validatePhone(phone, telephone, phoneRegex, msgNumber, msgPhone, msgCell);
    valMail = false;

    if (letters.test(name.value)) {
      document.getElementById("error-name").innerHTML = '';
      name.classList.remove('error');
      valName = true;
    } else {
      document.getElementById("error-name").innerHTML = name.value != '' ? msgName : msgRequired;
      name.classList.add('error');
      valName = false;
    }

    if (letters.test(surname.value)) {
      document.getElementById("error-surname").innerHTML = '';
      surname.classList.remove('error');
      valSurname = true;
    } else {
      document.getElementById("error-surname").innerHTML = surname.value != '' ? msgSurname : msgRequired;
      surname.classList.add('error');
      valSurname = false;
    }
    
    if (mailRegex.test(mail.value)) {
      document.getElementById("error-mail").innerHTML = '';
      mail.classList.remove('error');
      valMail = true;
    } else {
      document.getElementById("error-mail").innerHTML = mail.value != '' ? msgMail : msgRequired;
      mail.classList.add('error');
      valMail = false;
    }
    
    if(valName && valSurname && valPhone && valMail) {
      var successPanel = document.querySelector(".j-contactSuccess");
      successPanel.classList.add('success')
    }

  }

  function validatePhone(phone, telephone, phoneRegex, msgNumber, msgPhone, msgCell) {
    if (telephone.value === '' && phone.value === '') {
      phone.classList.remove('error');
      telephone.classList.remove('error');
      document.getElementById("error-telephone").innerHTML = msgNumber;
      telephone.classList.add('error');
      phone.classList.add('error');
      return false;
    } else {
      if (phoneRegex.test(telephone.value)) {
        document.getElementById("error-telephone").innerHTML = '';
        telephone.classList.remove('error');
        phone.classList.remove('error');
        return true;
      } else {
        document.getElementById("error-telephone").innerHTML = telephone.value != '' ? msgPhone : '';
        telephone.classList.add(telephone.value != '' ? 'error' : 'valid');
      }

      if (phoneRegex.test(phone.value)) {
        console.log('remove')
        document.getElementById("error-phone").innerHTML = '';
        phone.classList.remove('error');
        telephone.classList.remove('error');
        return true;
      } else {
        document.getElementById("error-phone").innerHTML = phone.value != '' ? msgCell : '';
        phone.classList.add(phone.value != '' ? 'error' : 'valid');
      }

      return false
    }
  }

  function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}