function getData() {
  var url = "./data/data.json";

  var xmlhttp = new XMLHttpRequest();

  xmlhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
          var result = JSON.parse(this.responseText);
          createSlider(result)
      }
  };
  xmlhttp.open("GET", url, true);
  xmlhttp.send();
}

function createSlider(data) {
  var slider = document.querySelector(".j-slider")    
    var html = '';

    var item = data.info
    for(var i = 0; i < item.length; i++) {
      var slideN = "slide"+(i+1) 

      var title = item[i][slideN.toString()][0].title;
      var subTitle = item[i][slideN.toString()][1].subTitle;
      var image = item[i][slideN.toString()][2].image;

      html += "<div class='swiper-slide'>\n<div class='slider__textWrappper'>\n<div class='slider__title'>"+title+"</div><div class='slider__text'>"+subTitle+"</div></div><div class='slider__wrapperImg'><img class='slider__img' src='images/"+image+"' alt='"+title+"'></div></div>";
    }

    slider.innerHTML = html

    var mySwiper = new Swiper('.swiper-container', {
      initialSlide: 1,
      loop: true,
      slidesPerView: 1.8,
      centeredSlides: true,
      speed: 1000,
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },

      
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    })

}

function failData() {
   var slider = document.querySelector(".j-slider")    
   var html = '';
   html += "<div class='slider__error'>Errore nel caricamento dei dati...</div>";
   slider.innerHTML = html
}

function initVideoOverlay() {

  var onpenVideoBtn = document.querySelector(".j-openVideo")
  var closeVideoBtn = document.querySelector(".j-closeVideo")
  var video = document.getElementById("video-1");
  
  onpenVideoBtn.addEventListener("click", function (){
    displayVideo();
  });
  closeVideoBtn.addEventListener("click", function (){
    closeVideo();
  });
  
  function displayVideo() {
    var video = document.querySelector(".j-videoOverlay");
    video.classList.add("visible");
    document.body.style.overflowY = "hidden";
    playVid()
  }
  function closeVideo() {
    var video = document.querySelector(".j-videoOverlay");
    video.classList.remove("visible");
    document.body.style.overflowY = "auto";
    pauseVid()
  }

  function playVid() { 
    video.play(); 
  } 
  
  function pauseVid() { 
    video.pause(); 
  } 
}


function initAnmations() {
  gsap.registerPlugin(ScrollTrigger);

  ScrollTrigger.defaults({
    //markers: true,
    scrub: 1,
  });
  
  
  gsap.to(".hero__logoSport", {
    duration: 2, 
    delay: .1,
    opacity: 1
  });
  gsap.to(".hero__logoSky", {
    duration: 2, 
    delay: .1,
    opacity: 1
  });
  gsap.to(".hero__title", {
    duration: 2, 
    delay: .1,
    opacity: 1
  });
  
  gsap.to(".hero__cta", {
    duration: 2, 
    delay: .3,
    opacity: 1
  });
  
  gsap.to(".description__par", {
    scrollTrigger: {
      trigger: ".hero__title",
      start: "top 10%",
      end: "bottom 10%", 
      //markers: true,
    }, 
    duration: 1, 
    opacity: 1, 
    ease: "none"
  });
}

(function() {
  getData();
  initVideoOverlay()
  initAnmations()
})();